package cjz.server1.constant;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStreamReader;

public class Constant
{
	public final static String SpiltLine = "\n-------------------------------------------------------------------------------\n";
	public final static int GetRequestStringStartLocation=4;
	public final static int PostRequestStringStartLocation=5;
	/**可以保存多少条缓存**/
	public final static int FileCacheKeepItemSize = 500;
	/**每条缓存可以有多大（MB）**/
	public final static int FileCacheSingeItemMaxSize_MB = 5;
	public final static int FileCacheSituationShowTime_ms = 3600 * 1000;
	public final static int CleanTimeCycle_ms = 3600 * 1000;
	public static String ServerPort = "9988";
	public static String ServerIPAddress = "127.0.0.1";
	public final static String SpecialFileName[] = {"/cjzblog"};
	public final static String IndexFileName = /*"/index.html"*/"/cjzblog";
	public static String HtmlLocation = "./html";
	public final static FilenameFilter fileNameFilter = new FilenameFilter() 
	{
        public boolean accept(File dir, String name)
        {
           if(name.lastIndexOf('.')>0)
           {
              // get last index for '.' char
              int lastIndex = name.lastIndexOf('.');
              
              // get extension
              String str = name.substring(lastIndex);
              
              // match path name extension
//              if(str.equals(".htm") || str.equals(".html") || str.equals(".pdf") || str.equals(".PDF") || str.equals(".rmvb") || str.equals(".RMVB") || str.equals(".mkv") ||str.equals(".MKV"))
              if(str.equals(".mp4"))
              {
                 return true;
              }
           }
           return false;
        }
     };
	public static String initConstan()
	{
		//setHtmlLocation();
		return HtmlLocation;
	}
	public static void init()
	{
		setHtmlLocation();
	}
	
	//异步加载配置文件项，以免读取路径时让浏览器等待时间变长
	private static void setHtmlLocation()
	{
		new Thread(new Runnable()
		{
			
			public void run()
			{
				File file;
				FileInputStream inputStream;
				InputStreamReader inputStreamReader;
				BufferedReader bufferedReader;
				while(true)
				{
					try
					{
						if( ! (file = new File("HtmlLocation.conf")).exists())
							System.out.println("配置文件失踪");
						inputStream = new FileInputStream(file);
						inputStreamReader = new InputStreamReader(inputStream);
						bufferedReader = new BufferedReader(inputStreamReader);
						HtmlLocation = bufferedReader.readLine();
						ServerPort = bufferedReader.readLine();
						bufferedReader.close();
						inputStreamReader.close();
						inputStream.close();
						Thread.sleep(2000);
					} catch (FileNotFoundException e)
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e)
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (InterruptedException e)
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}).start();
	}
}
