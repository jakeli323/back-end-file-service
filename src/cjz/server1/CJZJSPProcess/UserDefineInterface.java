package cjz.server1.CJZJSPProcess;

import java.io.BufferedOutputStream;

import cjz.server1.app.HttpRequestHeaderObject;

/**用户通过实现该接口，从而使得可以完全通过自己的意志来动态生成页面、处理get和post请求。实现完全的控制**/
public interface UserDefineInterface
{
	public void getInfoThenProcess(String HttpHeaderRequestSubPathAndFileName,HttpRequestHeaderObject headerObject,BufferedOutputStream bufferedOutputStream);
}
