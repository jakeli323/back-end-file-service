package cjz.server1.CJZJSPProcess;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;

import cjz.server1.app.ReadHtmlFileCache;
import cjz.server1.app.SendHttpOKMsg;
import cjz.server1.app.HttpRequestHeaderObject;
import cjz.server1.constant.Constant;

public class ReadAndSendCJZJSPFile 
{
	public boolean ReadAndSendCJZJSPFileFun(String FilePath,String SubPathAndFileName,BufferedOutputStream bufferedOutputStream, ReadHtmlFileCache fileCache,HttpRequestHeaderObject headerObject) 
	{
		if(SubPathAndFileName == null) return false;
		String FileNameWithoutOtherThings = null;
		try{
			FileNameWithoutOtherThings = SubPathAndFileName.substring(1, SubPathAndFileName.indexOf('.'));
		}
		catch(Exception e){
			e.printStackTrace();
			return false;
		}
		System.out.println("cjzjsp文件名: "+FileNameWithoutOtherThings);
		String StrArr[] = SubPathAndFileName.split("\\."); //一定要加双斜杠，不然分割失败
		for(String temp:StrArr)
		{
			if(temp.equals("CJZJSP") || temp.equals("cjzjsp"))
			{
				try
				{
					new SendHttpOKMsg(bufferedOutputStream);
					//下面的forname参数要改为根据用户的页面变化而变化
					//Class define_interface =  Class.forName("D:\\安卓工程\\运行测试\\CJZ服务器套件\\html\\Index_page");   //class.forname只能用于本工程内的类
					//String tempAbsPath = new File(FilePath).getAbsolutePath();
					//System.out.println("绝对路径为:" + tempAbsPath);
					//ClassLoader loader = new URLClassLoader(new URL[]{new URL("file://" + tempAbsPath + "/")}); //注意这个URL，这个方法第一次访问的时候会奇慢
					File fileCjzjsp = new File(FilePath);
					if(!fileCjzjsp.exists()) return false;
					URLClassLoader loader = new URLClassLoader(new URL[]{fileCjzjsp.toURI().toURL()}); //这个方法会快很多倍
					@SuppressWarnings("rawtypes")
					Class defineInterface = loader.loadClass(FileNameWithoutOtherThings);
					//newInstance新“实例化”，创建反射类型的对象
					UserDefineInterface f = (UserDefineInterface) defineInterface.newInstance();
					//如果f时UserDefineInterface的一个对象
					if(f instanceof UserDefineInterface)
						f.getInfoThenProcess(Constant.HtmlLocation + '/' + SubPathAndFileName, headerObject, bufferedOutputStream);
					loader.close();
					return true;
				} catch (ClassNotFoundException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IllegalAccessException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (InstantiationException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (MalformedURLException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return false;
	}
}
