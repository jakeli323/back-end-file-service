package cjz.server1.app;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import cjz.server1.constant.Constant;

/***特殊文件名数据处理，如果检测到特殊文件名为真，做专门的特殊处理，然后再输出**/
public class SpecialFileNameProccessor
{
	public boolean SpecialFileNameProccessorFun(String temp,BufferedOutputStream bufferedOutputStream)
	{
		if (temp == null || bufferedOutputStream == null) return false;
		else if(temp.equals(Constant.SpecialFileName[0])) return cjzblog(bufferedOutputStream);
		else return false;
	}
	
	//测试：
	/*@SuppressWarnings("unchecked")
	private boolean cjzblog(BufferedOutputStream bufferedOutputStream)
	{
		System.out.println("特殊文件名，做实时生成处理：");
		@SuppressWarnings("rawtypes")
		ArrayList DataArrayList = new ArrayList();
		DataArrayList.add(Constant.SpecialFileName[0]);
		sendData(DataArrayList, bufferedOutputStream);
		return true;
	}*/
	
	/*@SuppressWarnings("resource")
	private boolean cjzblog(BufferedOutputStream bufferedOutputStream)
	{
		//获取文件夹内容，生成超链接，并通过输出流输出
		File file;
		InputStream inputStream;
		InputStreamReader inputStreamReader;
		BufferedReader bufferedReader;
		ArrayList<String> htmllist = new ArrayList<String>();
		String temp;
		try
		{
			if( ! (file = new File(Constant.GetHtmlLocation() + "/index.html")).exists()) return false;
			inputStream = new FileInputStream(file);
			inputStreamReader = new InputStreamReader(inputStream);
			bufferedReader = new BufferedReader(inputStreamReader);
			while( (temp = bufferedReader.readLine()) != null)
				htmllist.add(temp + '\0');
			sendData(htmllist, bufferedOutputStream);
		}catch (FileNotFoundException e)
		{
			e.printStackTrace();
		} catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;
	}*/
	
	private boolean cjzblog(BufferedOutputStream bufferedOutputStream)
	{
		//获取文件夹内容，生成超链接，并通过输出流输出
		ArrayList<String> htmllist = new ArrayList<String>();
		File file = new File(Constant.initConstan());
		String[] fileArray = file.list(Constant.fileNameFilter);
		if(fileArray == null) return false;
		if(fileArray.length <= 0) return false;
		htmllist.add("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \" http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n");
		htmllist.add("<html xmlns=\" http://www.w3.org/1999/xhtml\">\n");
		htmllist.add("<head>\n");
		htmllist.add("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=GBK\" />\n");
		JSONArray jsonArray = new JSONArray();
		for(String temp:fileArray)
		{
//			htmllist.add("<h1><a href=\"" + temp + "\">" + temp.substring(0,lastIndex) + "</a><br></h1>");
			try {
				int lastIndex = temp.lastIndexOf('.');
				JSONObject jsonData = new JSONObject();
				jsonData.put("URL", "http://" + Constant.ServerIPAddress + ":" + Constant.ServerPort + "/" + temp);
				jsonData.put("Name", temp.substring(0,lastIndex));
				jsonArray.put(jsonData);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		htmllist.add(jsonArray.toString());
		sendData(htmllist, bufferedOutputStream);
		return true;
	}


	@SuppressWarnings("rawtypes")
	private void sendData(ArrayList Data,BufferedOutputStream bufferedOutputStream)
	{
 		try
		{
			new SendHttpOKMsg(bufferedOutputStream);
			for(int i=0; i<Data.size(); i++)
			{
				try{   bufferedOutputStream.write(((String)Data.get(i)).getBytes());}   catch (IOException e)   { System.err.println("写出数据掉包");}
			}
			bufferedOutputStream.flush();
			bufferedOutputStream.close();
		} catch (IOException e)
		{
			//e.printStackTrace();
			System.err.println("写出数据失败");
		}
	}
}

//Class.forName(arg0); //以后可以通过读取配置文件，来确定使用哪个接口或者类去处理特殊文件名
