package cjz.server1.app;

import java.io.File;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import cjz.server1.constant.Constant;

/**网页王建缓存机制*/
public class ReadHtmlFileCache
{
	
	public ArrayList<CJZServerFileObject> FileCache;
	public ReadHtmlFileCache()
	{
		PrepareReadHtml();
		CacheRubbitshCleaner.start();
		CacheSituation.start();
	}
	
	/**缓存根目录网页文件，提高他们第一次打开的速度**/
	private void PrepareReadHtml()
	{
		FileCache = new ArrayList<CJZServerFileObject>();
		try
		{
			PrepareRead();
		} catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("resource")
	private void PrepareRead() throws IOException
	{
		File file = new File(Constant.initConstan());
		FilenameFilter fileNameFilter = new FilenameFilter() 
		{
            public boolean accept(File dir, String name)
            {
               if(name.lastIndexOf('.')>0)
               {
                  // get last index for '.' char
                  int lastIndex = name.lastIndexOf('.');
                  
                  // get extension
                  String str = name.substring(lastIndex);
                  
                  // match path name extension
                  if(str.equals(".htm") || str.equals(".html"))
                  {
                     return true;
                  }
               }
               return false;
            }
         };
		String[] fileArray = file.list(fileNameFilter);
		if(fileArray == null) return;
		if(fileArray.length==0) return;
		for(String tempFileName:fileArray)
		{
			File tempFile = new File(tempFileName);
			InputStream inputStream;
			inputStream = new FileInputStream(Constant.initConstan() + '/' + tempFileName);
			int tempByte;
			CJZServerFileObject fileObject = new CJZServerFileObject();
//			fileObject.FileName = tempFileName;
			fileObject.FileName = Constant.initConstan() + '/' + tempFileName;
			fileObject.lastModified = tempFile.lastModified();
			while ((tempByte = inputStream.read()) != -1) 
			{
				fileObject.fileDataArrayList.add(tempByte);
            }
			FileCache.add(fileObject);
			System.out.println(tempFileName + " 文件已读取缓存");
		}
	}
	
	
	//写一个线程，1小时执行一次，把读取次数ReadCount次数最多的FileCacheKeepItemSize个保留，其余全部清除
	Thread CacheRubbitshCleaner = new Thread(new Runnable()
	{
		
		public void run()
		{
			int MinReadCount = 0;
			int MinReadItemPosition = 0;
			while(true)
			{
				try
				{
					Thread.sleep(Constant.CleanTimeCycle_ms);
					MinReadCount = (FileCache.get(0)==null)?0:FileCache.get(0).ReadCount;
					if(FileCache.size()<=1) continue; //只有一条东西在缓存中那还清什么
					while(FileCache.size() > Constant.FileCacheKeepItemSize)
					{
						for(int i=1;i<FileCache.size();i++)
						{
							if(FileCache.get(i).ReadCount < MinReadCount)
							{
								MinReadCount = FileCache.get(i).ReadCount;
								MinReadItemPosition = i;
							}
						}
						FileCache.remove(MinReadItemPosition);
						//清0，重新计算
						for(int i=1;i<FileCache.size();i++)		FileCache.get(i).ReadCount = 0;
					}
					MinReadCount = FileCache.get(0).ReadCount;
					MinReadItemPosition = 0;
				} catch (InterruptedException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	});
	
	//缓存情况:
	Thread CacheSituation = new Thread(new Runnable()
	{
		
		public void run()
		{
			while(true)
			{
				try
				{
					Thread.sleep(Constant.FileCacheSituationShowTime_ms);
					System.out.println("\n缓存文件量:"+FileCache.size());
					for(int i=0;i<FileCache.size();i++)
					{
						System.out.println("被缓存文件" + i + ": " + FileCache.get(i).FileName + "被读取" + FileCache.get(i).ReadCount + "次");
					}
				} catch (InterruptedException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	});
}
