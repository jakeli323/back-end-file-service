package cjz.server1.app;

import java.io.BufferedOutputStream;
import java.util.ArrayList;

import cjz.server1.CJZJSPProcess.ReadAndSendCJZJSPFile;
import cjz.server1.constant.Constant;

public class SendWebsitesData
{
	private String HttpHeaderRequestSubPathAndFileName;
	public SendWebsitesData(ArrayList<String> ClientSendedHttpHeader,BufferedOutputStream bufferedOutputStream, ReadHtmlFileCache fileCache)
	{
		//从http头众多信息中，解释文件名，并载入到内存中（在htmlList数组表里）
		HttpHeaderCheck headerCheck = new HttpHeaderCheck(); 
		HttpHeaderRequestSubPathAndFileName = headerCheck.HttpHeaderCheckFun(ClientSendedHttpHeader); 
		HttpRequestHeaderObject headerObject = headerCheck.GetheaderObject();
		//对Get或Post过来的参数做回应（代码预留区）：
		if(new GetAndPostProccess().ProccessFun(HttpHeaderRequestSubPathAndFileName,headerObject)==true) return;
		//特殊文件名数据处理，如果检测到特殊文件名为真，做专门的特殊处理，然后再输出。直接在本类的该句结束，不做普通文件处理。
		if(new SpecialFileNameProccessor().SpecialFileNameProccessorFun(HttpHeaderRequestSubPathAndFileName,bufferedOutputStream)==true) return;
//		if(new SpecialFileNameProccessor2().SpecialFileNameProccessorFun(HttpHeaderRequestSubPathAndFileName,bufferedOutputStream)==true) return;
		//非html文件按照文件类型加ContentType Http头进行下载:
//		if(new ReadAndSendTypeFile().ReadAndSendTypeFileFun(Constant.initConstan(),HttpHeaderRequestSubPathAndFileName, bufferedOutputStream, fileCache, headerObject) == true) return;
		//检查是否是CJZJSP后序文件
		if(new ReadAndSendCJZJSPFile().ReadAndSendCJZJSPFileFun(Constant.initConstan(),HttpHeaderRequestSubPathAndFileName, bufferedOutputStream, fileCache, headerObject) == true) return;
		//否则就是普通文件，直接读取并输出
		new ReadAndSendHtmlFile().ReadHtmlFileFun(Constant.initConstan()+HttpHeaderRequestSubPathAndFileName, bufferedOutputStream,fileCache);
	}
}
