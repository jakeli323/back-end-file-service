package cjz.server1.app;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;

import cjz.server1.constant.Constant;

public class HttpHeaderCheck
{
	/***分析http协议头，连接服务端想要什么*/
	HttpRequestHeaderObject headerObject = null;
	public String HttpHeaderCheckFun(ArrayList<String> clientSendedHttpHeader)
	{
		try
		{
			if(clientSendedHttpHeader.size() == 0) return null;
			/*根据get和post传过来的东西，返回决定加载哪个页面*/
			if(URLDecoder.decode(clientSendedHttpHeader.get(0).substring(0, 3),"utf-8").equals("GET")) 
				return CheckGetString(clientSendedHttpHeader);
			//post数据最好多做一个端口，不然有时候其他网页加载过程中会莫名得到post回去浏览器的数据
			else if(URLDecoder.decode(clientSendedHttpHeader.get(0).substring(0, 4),"utf-8").equals("POST")) 
				return CheckPostString(clientSendedHttpHeader);
			else 
				return null;
		} catch (UnsupportedEncodingException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	/**Get方法需要的网页名字在4个字符后面，Post则为5个字符开始，到？或者 空格 结束*/
	private String CheckGetString(ArrayList<String> clientSendedHttpHeader)
	{
		
		int RequestStringEnd=Constant.GetRequestStringStartLocation,GetMethodRequest=0;
		String clientwant,GetMethodString=null;
		while(clientSendedHttpHeader.get(0).charAt(RequestStringEnd)!=' ') 
		{
			RequestStringEnd++;
			if(clientSendedHttpHeader.get(0).charAt(RequestStringEnd)=='?') GetMethodRequest = RequestStringEnd;
		}
		try
		{
			if(GetMethodRequest != 0) 
			{
				clientwant = URLDecoder.decode(clientSendedHttpHeader.get(0).substring(Constant.GetRequestStringStartLocation, GetMethodRequest), "utf-8");  //如果地址中有get方法，要在？那里结束文件名
				GetMethodString = URLDecoder.decode(clientSendedHttpHeader.get(0).substring(GetMethodRequest+1, RequestStringEnd), "utf-8");
				System.out.println("客户端用了get方法传了这些东西过来：" + GetMethodString);
				headerObject = new GetMethodProccessor().GetMethodProccessorFun(GetMethodString);
				headerObject.HttpWantFile = clientwant;
			}
			else 
			{
				clientwant = URLDecoder.decode(clientSendedHttpHeader.get(0).substring(Constant.GetRequestStringStartLocation, RequestStringEnd), "utf-8");  //否则直接用整个文件名即可
			}
			System.out.println("客户端想得到："+clientwant);
			if(clientwant.equals("/")) return Constant.IndexFileName; //以后改为如果访问index.html，就列出html文件夹里面htm和html类型的文件列表，动态生成Html页面并为地址加上超连接
			else	return clientwant;
		} catch (UnsupportedEncodingException e)
		{
			e.printStackTrace();
		}
		return null;
	}
	
	private String CheckPostString(ArrayList<String> clientSendedHttpHeader)
	{
		int RequestStringEnd=Constant.PostRequestStringStartLocation;
		String clientwant = null;
		try
		{
			while(clientSendedHttpHeader.get(0).charAt(RequestStringEnd)!=' ') 
			{
				RequestStringEnd++;
			}
			clientwant = URLDecoder.decode(clientSendedHttpHeader.get(0).substring(Constant.PostRequestStringStartLocation, RequestStringEnd), "utf-8");
			System.out.println("客户端post信息到什么页面："+clientwant);
			System.out.println("Post字符串: " + clientSendedHttpHeader.get(clientSendedHttpHeader.size() - 1));
			headerObject = new PostMethodProccessor().PostMethodProccessorFun(clientSendedHttpHeader.get(clientSendedHttpHeader.size() - 1));
			headerObject.HttpWantFile = clientwant;
			if(clientwant.equals("/"))  return  Constant.IndexFileName; //以后改为如果访问index.html，就列出html文件夹里面htm和html类型的文件列表，动态生成Html页面并为地址加上超连接
			else if(clientwant != null) return clientwant;
		} catch (UnsupportedEncodingException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public HttpRequestHeaderObject GetheaderObject()
	{
		return headerObject;
	}
}
