package cjz.server1.app;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import cjz.server1.constant.Constant;

public class ReadAndSendHtmlFile
{
	private byte[] tempDataCache;

	/****
	 * 得到路径之后读出文件数据，并传回客户端**
	 * @author 陈杰柱
	 * @param fileCache */
	
	public void ReadHtmlFileFun(String FilePath,BufferedOutputStream bufferedOutputStream, ReadHtmlFileCache fileCache)
	{
		File file;
		InputStream inputStream;
		CJZServerFileObject fileObject = null;
		try
		{
			//文件不存在则直接退出
			if( ! (file = new File(FilePath)).exists()) 
			{
				System.out.println("客户端请求无效文件");
				return ;
			}
			//文件在缓存中则从缓存中读取，然后直接结束。
			for(int i=0;i<fileCache.FileCache.size();i++)
			{
				if(FilePath.equals(fileCache.FileCache.get(i).FileName))
				{
					System.out.println(FilePath + "在缓存中");
					//如果文件程度不一致，则绝对是被修改过，将它从缓存中剔除
					if(ClearChangedItemOnCache(i, file, fileCache) == true) break;
					//从缓存中给客户端发数据
					ReadHtmlFileOnCache(bufferedOutputStream, fileCache.FileCache.get(i).getFileArrayList(),fileCache.FileCache.get(i).getFileArrayList().size());
					return ;
				}
			}
			//文件存在，而且不在缓存中。逐行读取文件内容，并输出到流中，基于浏览器，过程中顺便把该文件写入到缓存中
			inputStream = new FileInputStream(file);
			long FileLength = file.length();
			new SendHttpOKMsg(bufferedOutputStream,FileLength);
			//新添加到缓存的文件必须加路径，以免同名文件冲突
			if( (FileLength/1024/1024) < Constant.FileCacheSingeItemMaxSize_MB)  
			{	
				fileObject = new CJZServerFileObject();  //添加过程中避免添加过大的文件。例如不能大如5MB file.length(MB)
				fileObject.FileName = FilePath;
				fileObject.lastModified = file.lastModified();
				fileObject.ReadCount ++ ;
				System.out.println("fileObject.FileName:"+fileObject.FileName);
				tempDataCache = new byte[1024*8];
				long inputStreamByteCount = 0;
				while ((inputStream.read(tempDataCache)) != -1) 
				{
					for(byte temp:tempDataCache) //tempDataCache在最后一行时可能会有垃圾byte，根据文件长度废弃之
					{	
						bufferedOutputStream.write(temp);
						fileObject.fileDataArrayList.add((int)(temp));
						inputStreamByteCount++; //已输出的字节数
						if(inputStreamByteCount==FileLength) break;
					}
	            }
				fileCache.FileCache.add(fileObject);
			}
			else 
			{
				System.out.println("文件:"+file.getName()+" 过大，将不放入缓存中");
				//这种方法飞快但耗资源，只有某些连接在请求大文件的时候才用
				tempDataCache = new byte[1024*1024*8]; //快缓存越大，对方下载速度越快，直到达到带宽上限 或 磁盘速度上限。但并不是越大越好，否则大文件下载连接一多，服务器的内存消耗会非常大。而且可能会时快时慢（因为要等待硬盘塞满块缓存再写出去）
				long inputStreamByteCount = 0;
				while ((inputStream.read(tempDataCache)) != -1) 
				{
					for(byte temp:tempDataCache)
					{	
						bufferedOutputStream.write(temp);
						inputStreamByteCount++; //已输出的字节数
						if(inputStreamByteCount==FileLength) break;
					}
	            }
			}
			inputStream.close();
			bufferedOutputStream.flush();
		} catch (FileNotFoundException e)
		{
			e.printStackTrace();
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	
	/**从缓存中读取文件内容**/
	private void ReadHtmlFileOnCache(BufferedOutputStream bufferedOutputStream,ArrayList<Integer> Data,int DataSize)
	{
		try
		{
			new SendHttpOKMsg(bufferedOutputStream,DataSize);
			for(int i=0;i<Data.size();i++)
			{
				bufferedOutputStream.write(Data.get(i));	
	        }
			bufferedOutputStream.flush();
		} catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**检查磁盘的文件和缓存的修改日期、大小有没有区别，任意一个有区别都清理该条**/
	private boolean ClearChangedItemOnCache(int itemPosition,File file,ReadHtmlFileCache fileCache)
	{
		long FileLength = file.length();
		//如果文件长度不一致或者修改时间不一样，则绝对是被修改过，将它从缓存中剔除
		if(FileLength == fileCache.FileCache.get(itemPosition).fileDataArrayList.size() && file.lastModified() == fileCache.FileCache.get(itemPosition).lastModified)
		{
			System.out.println("缓存网页文件未被更新");
		}
		else
		{
			System.out.println("缓存网页文件已经被更新!!!  将删除该文件的缓存，并将其重新加入缓存中!!!");
			System.out.println("filecache中的大小为:"+fileCache.FileCache.get(itemPosition).fileDataArrayList.size() + "文件则是:"+FileLength);
			System.out.println("filecache中的修改为:"+fileCache.FileCache.get(itemPosition).lastModified + "文件则是:"+file.lastModified());			
			fileCache.FileCache.remove(itemPosition);
			return true;
		}
		return false;
	}
}
