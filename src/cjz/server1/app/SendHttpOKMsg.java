package cjz.server1.app;

import java.io.BufferedOutputStream;
import java.io.IOException;

/***根据形参不同发送不同的Http头***/
public class SendHttpOKMsg
{
	public SendHttpOKMsg(BufferedOutputStream bufferedOutputStream)
	{
		try
		{
			bufferedOutputStream.write("HTTP/1.1 200 OK\n".getBytes());
			bufferedOutputStream.write("Server: CJZ_SERVER V1.0\n".getBytes());
			//bufferedOutputStream.write(("Content-Length: " + htmlList.size() + "\n").getBytes());  //要么没有，要么长度一定要准确
			//bufferedOutputStream.write("Connection: keep-alive\n".getBytes());
			//bufferedOutputStream.write("Content-Type: text/html\n".getBytes());
			//bufferedOutputStream.write("Date: Thu, 05 Feb 2015 07:39:09 GMT\n".getBytes());
			//一定要有空字符作为结束，不然加载图片之类的一定扑街一定扑街
			bufferedOutputStream.write("\n".getBytes());
			bufferedOutputStream.write("".getBytes());				
			bufferedOutputStream.flush();
		} catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	public SendHttpOKMsg(BufferedOutputStream bufferedOutputStream,long size)
	{
		try
		{
			bufferedOutputStream.write("HTTP/1.1 200 OK\n".getBytes());
			bufferedOutputStream.write("Server: CJZ_SERVER V1.0\n".getBytes());
			bufferedOutputStream.write(("Content-Length: " + size + "\n").getBytes());  //要么没有，要么长度一定要准确
			SendHttpOkMsgFinish(bufferedOutputStream);
		} catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	public SendHttpOKMsg(BufferedOutputStream bufferedOutputStream, String contentType, long size)
	{
		try
		{
			bufferedOutputStream.write("HTTP/1.1 200 OK\n".getBytes());
			bufferedOutputStream.write("Server: CJZ_SERVER V1.0\n".getBytes());
			bufferedOutputStream.write(("Content-Type: " + contentType + "\n").getBytes());
			bufferedOutputStream.write(("Content-Length: " + size + "\n").getBytes());  //要么没有，要么长度一定要准确
			SendHttpOkMsgFinish(bufferedOutputStream);
		} catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	public SendHttpOKMsg(BufferedOutputStream bufferedOutputStream,long size,String File_after_the_sequence_name)
	{
		try
		{
			bufferedOutputStream.write("HTTP/1.1 200 OK\n".getBytes());
			bufferedOutputStream.write("Server: CJZ_SERVER V1.0\n".getBytes());
			bufferedOutputStream.write(("Content-Length: " + size + "\n").getBytes());  //要么没有，要么长度一定要准确
			if(File_after_the_sequence_name.equals("htm") || File_after_the_sequence_name.equals("html")) 
				bufferedOutputStream.write("Content-Type: text/html\n".getBytes());
			SendHttpOkMsgFinish(bufferedOutputStream);
		} catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	private void SendHttpOkMsgFinish(BufferedOutputStream bufferedOutputStream) throws IOException
	{
		//一定要有空字符作为结束，不然加载图片之类的一定扑街一定扑街
		bufferedOutputStream.write("\n".getBytes());
		bufferedOutputStream.write("".getBytes());				
		bufferedOutputStream.flush();
	}

}
