package cjz.server1.app;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

import cjz.server1.constant.Constant;

public class cjz_server_main
{
	private static boolean loop1_Is_Busy = false;
	private static boolean loop2_Is_Busy = false;	
	private static boolean loop3_Is_Busy = false;	
	private static ReadHtmlFileCache fileCache;
	private static ExecutorService fixedThreadPool;
	private static final int threadPoolSize = Runtime.getRuntime().availableProcessors() * 100; //线程池大小为 核心数 * 100
	public static void main(String args[])
	{
		Constant.init();
		ServerSocket serverSocket = null;
		fixedThreadPool = Executors.newFixedThreadPool(threadPoolSize);  
		System.out.println("核心数  : " + Runtime.getRuntime().availableProcessors());
		System.out.println("物理内存+虚拟内存 (GB) : "+Runtime.getRuntime().totalMemory()/1024/1024);
		try
		{
			serverSocket = new ServerSocket(Integer.valueOf(Constant.ServerPort));
			Constant.ServerIPAddress = serverSocket.getInetAddress().getLocalHost().getHostAddress();
			//预先加载根目录的html文件，使得第一次访问这些文件的时候便是最快的显示速度
			fileCache = new ReadHtmlFileCache();
		} catch (IOException e2)
		{
			e2.printStackTrace();
		}
		while(true)
		{
			if(!loop1_Is_Busy) loop1(serverSocket); //有时候没accept到的连接会无法获取数据（被漏掉了）
			else if(!loop2_Is_Busy) loop2(serverSocket);
			else if(!loop3_Is_Busy) loop3(serverSocket);
			else loop1(serverSocket);
		}
		
	}
	
	private static void loop1(ServerSocket serverSocket)
	{
		loop1_Is_Busy = true;
		//在内存钟开辟一个新的地址存放serversocket返回的socket，以防所有线程都用了同一个引用产生错乱
		final Socket socket;
		try
		{
			//在内存钟开辟一个新的地址存放serversocket返回的socket，以防所有线程都用了同一个引用产生错乱
			socket = serverSocket.accept();
			fixedThreadPool.execute(new Runnable()
			{
				
				public void run()
				{
					new SocketClientResponse(socket,fileCache);
					loop1_Is_Busy = false;
				}
			});
		} catch (IOException e)
		{
			try
			{
				serverSocket.close();
			} catch (IOException e1)
			{
				e1.printStackTrace();
			}
			e.printStackTrace();
		}
		
	}
	
	private static void loop2(ServerSocket serverSocket)
	{
		loop2_Is_Busy = true;
		final Socket socket;
		try
		{
			socket = serverSocket.accept();
			fixedThreadPool.execute(new Runnable()
			{
				
				public void run()
				{
					new SocketClientResponse(socket,fileCache);
					loop2_Is_Busy = false;
				}
			});
		} catch (IOException e)
		{
			try
			{
				serverSocket.close();
			} catch (IOException e1)
			{
				e1.printStackTrace();
			}
			e.printStackTrace();
		}
		
	}
	
	private static void loop3(ServerSocket serverSocket)
	{
		loop3_Is_Busy = true;
		final Socket socket;
		try
		{
			socket = serverSocket.accept();
			fixedThreadPool.execute(new Runnable()
			{
				
				public void run()
				{
					new SocketClientResponse(socket,fileCache);
					loop3_Is_Busy = false;
				}
			});
		} catch (IOException e)
		{
			try
			{
				serverSocket.close();
			} catch (IOException e1)
			{
				e1.printStackTrace();
			}
			e.printStackTrace();
		}
		
	}
}
