package cjz.server1.app;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import cjz.server1.constant.Constant;

public class SpecialFileNameProccessor2 {

	public boolean SpecialFileNameProccessorFun(String httpHeaderRequestSubPathAndFileName,BufferedOutputStream bufferedOutputStream) {
		return cjzblog(bufferedOutputStream);
	}
	
	private boolean cjzblog(BufferedOutputStream bufferedOutputStream)
	{
		//获取文件夹内容，生成超链接，并通过输出流输出
		File file = new File(Constant.initConstan());
		String[] fileArray = file.list(Constant.fileNameFilter);
		if(fileArray == null) return false;
		if(fileArray.length <= 0) return false;
		System.out.println("文件数目:" + fileArray.length);
		JSONArray jsonDataArray = new JSONArray();
		for(String temp : fileArray){
			try {
				JSONObject itemJson = new JSONObject();
				int lastIndex = temp.lastIndexOf('.');
				itemJson.put("URL", "http://192.168.1.200:" + Constant.ServerPort + "/" + temp);
				itemJson.put("Name", temp.substring(0,lastIndex));
				jsonDataArray.put(itemJson);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		try {
			bufferedOutputStream.write((jsonDataArray.toString()).getBytes());
			bufferedOutputStream.flush();
			bufferedOutputStream.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;
	}

}
