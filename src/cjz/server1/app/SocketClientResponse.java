package cjz.server1.app;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.ArrayList;

import cjz.server1.constant.Constant;

public class SocketClientResponse
{
	ReadHtmlFileCache fileCache;
	public SocketClientResponse(Socket socket, ReadHtmlFileCache fileCache)
	{
		this.fileCache = fileCache;
		SocketClientResponser(socket);
	}
	
	private void SocketClientResponser(Socket socket)
	{
		final Socket tempSocket = socket;
		/*Thread socketThread = new Thread(new Runnable()
		{
			
			public void run()
			{*/
				BufferedOutputStream bufferedOutputStream;
				InputStreamReader inputStreamReader;
				BufferedReader bufferedReader;
				ArrayList<String> arrayListClientSendedHttpHeader;
				try
				{
					/*初始化输入输出流*/
					bufferedOutputStream = new BufferedOutputStream(tempSocket.getOutputStream());
					inputStreamReader = new InputStreamReader(tempSocket.getInputStream());
					bufferedReader = new BufferedReader(inputStreamReader);
					/*获取http头有用没用的所有信息，并保存到数组表ClientSendedHttpHeader中*/
					arrayListClientSendedHttpHeader = new HttpGetRequestAndResponse().HttpGetRequestAndResponseFun(tempSocket, bufferedOutputStream);
					/*根据http头，分析它，并发送客户端请求的文件东西回客户端*/
					new SendWebsitesData(arrayListClientSendedHttpHeader, bufferedOutputStream, fileCache);
					/*收尾工作*/
					arrayListClientSendedHttpHeader.clear();
					bufferedReader.close();
					inputStreamReader.close();
					bufferedOutputStream.close();
					tempSocket.close();
					System.out.println(Constant.SpiltLine);
				} catch (IOException e)
				{
					//e.printStackTrace();
					System.err.println("输入输出过程出现错误，可能是客户端主动断开了连接");
				}
				
			/*}
		});
		socketThread.start();*/
	}
	
	
}
